/*  coveraudiod -- plays sounds while using surface touch covers
	Copyright (C) 2014 Javier S. Pedro <dev.git@javispedro.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>

#include <glib.h>

#include "x11.h"
#include "audio.h"

int main(int argc, char *argv[])
{
	GMainLoop *loop = g_main_loop_new(NULL, TRUE);

	if (!x11_init()) {
		return EXIT_FAILURE;
	}

	if (!audio_init()) {
		return EXIT_FAILURE;
	}

	g_debug("coveraudiod running");

	g_main_loop_run(loop);

	audio_close();
	x11_close();

	return EXIT_SUCCESS;
}
