/*  coveraudiod -- plays sounds while using surface touch covers
	Copyright (C) 2014 Javier S. Pedro <dev.git@javispedro.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "audio.h"
#include "key.h"

static unsigned int key_press_count = 0;

void handle_keypress(bool dead_key)
{
	int num_sample = 0;

	if (dead_key) {
		num_sample = (key_press_count % NUM_MOD_SAMPLES) + FIRST_MOD_SAMPLE;
	} else {
		num_sample = (key_press_count % NUM_KEY_SAMPLES) + FIRST_KEY_SAMPLE;
	}

	audio_play_sample(num_sample);

	key_press_count++;
}
