CFLAGS:=-Wall -O2 -ggdb

pkgmodules:=glib-2.0 libpulse-mainloop-glib sndfile x11 xi
includes:=$(shell pkg-config --cflags $(pkgmodules))
libs:=$(shell pkg-config --libs $(pkgmodules))
objs:=main.o x11.o audio.o key.o

all: coveraudiod

coveraudiod: $(objs)
	$(CC) -o $@ $+ $(libs)

$(objs): %.o: %.c
	$(CC) -std=c99 $(includes) $(CFLAGS) -o $@ -c $<

clean:
	rm -f *.o coveraudiod

install: coveraudiod
	install coveraudiod $(DESTDIR)/usr/bin
	install -d $(DESTDIR)/usr/share/coveraudiod
	install -m 0644 *.wav $(DESTDIR)/usr/share/coveraudiod/

.PHONY: all clean install

