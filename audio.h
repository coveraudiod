/*  coveraudiod -- plays sounds while using surface touch covers
	Copyright (C) 2014 Javier S. Pedro <dev.git@javispedro.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef AUDIO_H
#define AUDIO_H

#include <stdbool.h>

enum samples {
	FIRST_KEY_SAMPLE = 0,
	LAST_KEY_SAMPLE = 5,
	NUM_KEY_SAMPLES = 6,
	FIRST_MOD_SAMPLE = 6,
	LAST_MOD_SAMPLE = 7,
	NUM_MOD_SAMPLES = 2,
	NUM_SAMPLES = 8
};

bool audio_init();
void audio_close();

void audio_play_sample(int num_sample);

#endif // AUDIO_H
